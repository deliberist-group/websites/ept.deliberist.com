# 80% There

[![pipeline](https://gitlab.com/deliberist-group/websites/ept.deliberist.com/badges/main/pipeline.svg)](https://gitlab.com/deliberist-group/websites/ept.deliberist.com/pipelines)

> Learn 20% of a tool, reach 80% efficiency.

EPT, or **80% There** is a collection of articles describing how to use command-
line tools that help developers, sysadmins, researchers, and anyone finding
themselves using a terminal.

The guiding concept behind EPT is based on the
[Pareto Principle](https://en.wikipedia.org/wiki/Pareto_principle),
where if you can learn 20% of a tool's features then you will have enough
knowledge to use the tool and implement your 80% solution.  From there, EPT will
hopefully equip you with enough knowledge to implement the final 20% as you see
fit.

# Contributing

The best way to contribute changes to EPT is to fork the GitLab project, make
the changes local to your forked repository, then submit a Merge Request through
GitLab.

Although there is no formal coding or writing standards used with EPT, your
Merge Request is more likely to be successful if your changes are consistent
with the existing styles.  This includes indentations, variable/method names,
and even the amount of comments and documentation.

Merge Requests are also more likely to be successful if changed files are
covered under existing or new lint jobs in the continuous integration jobs.

Finally, the commands below are intended for the main developer of EPT, or
anyone given maintainer/developer rights on the GitLab project.

## Adding a new article

1. Create the markdown article under `src/articles/**/*.md`.
1. Update the navigation tree at `source/SUMMARY.md`.
1. Ensure all new files are covered under new/existing CI lint jobs.
1. Validate the new changes show up in the deployed GitLab Pages.

## Local Testing

First, install [mdbook](https://rust-lang.github.io/mdBook/guide/installation.html).

```bash
# Just build the project:
mdbook build

# Or build and serve it:
mdbook serve --open
```

## Images

For image generation, these websites ended up being quite helpful.  In
particular, I felt like they gave EPT and 80% solution with only 20% effort.

- [https://logohub.io/#](https://logohub.io/#)
- [https://favicon.io/favicon-converter/](https://favicon.io/favicon-converter/)

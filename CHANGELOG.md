<!-- markdownlint-disable-file no-duplicate-heading -->

# Non-Traditional Version Numbers

Since EPT is not really a tool, per se, but rather a website that is quite
literally deployed with each commit to Git main, explicit version numbers are
rather pointless.  Instead, this changelog will be date focused.

## 2024

### December

- Additional utility scripts added.
- CI pipeline enchangements.

### January

- Rewritten as an [mdbook](https://rust-lang.github.io/mdBook/) project.

## 2021

### January

- Archived original zips where the vendor specific assets came from.
- Fixed references to the old project name in [CONTRIBUTING.md](CONTRIBUTING.md).
- Fixed superfluous linter issues.
- Added configurable contact icons to the footer!
- Added initial [CODEOWNERS](.gitlab/CODEOWNERS) file.
- Adding linting of Ruby code.
- Disabled the template/skeleton pages that are not written yet, preparing for
  actually launching the site.
- Experimented with different styling changes.
- Fixed references to `ept` when they should have been `ept.deliberist.com`.
- Fixing the padding with lists, which had problems both when nested and
  not-nested.
- Shifted and added stubbed/skeleton articles in the article tree.
- Published [mouseless](./source/articles/mouseless.md).
- Added Reddit and Twitter contacts.
- Caching the Ruby Gem dependencies across all CI jobs.

## 2020

### December

- Fixed the `baseurl` config property after setting up a real DNS name.

### August

- Renamed the default branch from `master` to `main` to be more culturally
    inclusive.
- Updated the GitLab CI config to reflect the new default branch name.

### July

- Futzing with the `Gemfile`.

### June

- Initial version, which basically is only the infrastructure for the site with
    a basic skeleton for articles.

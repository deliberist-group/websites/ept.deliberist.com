#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

readonly project_dir="$(realpath -e "$(dirname "${BASH_SOURCE[0]}")/../")"
readonly project_name="$(basename "${project_dir}")"

readonly image="rust:1.83.0-alpine3.21"

readonly container_dir="/ept"
readonly container_cargo_home="${container_dir}/.cargo"
readonly uid="$(id -u)"
readonly gid="$(id -g)"

# Download the image.
docker image pull "${image}"

# Setup the container.
docker container create                             \
    --env CARGO_HOME="${container_cargo_home}"      \
    --interactive                                   \
    --name "${project_name}"                        \
    --network host                                  \
    --tty                                           \
    --volume "${project_dir}:${container_dir}:z"    \
    --workdir "${container_dir}"                    \
    "${image}"
docker container start "${project_name}"
function cleanup-container {
    docker container stop "${project_name}" || true
    docker container kill "${project_name}" || true
    docker container rm "${project_name}" || true
    yes | docker container prune || true
}
trap cleanup-container EXIT

# While we're root, install the necessary packages.
readonly dce_root="docker container exec --interactive --tty '${project_name}'"
eval "${dce_root}" apk add --no-cache musl-dev

# While we're non-root, install the necessary packages.
readonly dce_user="docker container exec --env CARGO_HOME='${container_cargo_home}' --interactive --tty --user '${uid}:${gid}' '${project_name}'"
eval "${dce_user}" cargo install mdbook

# Finally open the shell as the non-root user.
eval "${dce_user}" sh

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml

# Cache the cargo builds between jobs.
cache: &global_cache
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .cargo
    - target
  policy: pull-push

variables:
  # Redirect CARGO_HOME to the project directory, so it can be cached.
  CARGO_HOME: "${CI_PROJECT_DIR}/.cargo"
  IMAGE_JAVASCRIPT: "node:23.5.0-alpine3.21"
  IMAGE_RUST: "rust:1.83.0-alpine3.21"
  IMAGE_SHELLCHECK: "koalaman/shellcheck-alpine:v0.10.0"

stages:
  - lint
  - build
  - test
  - deploy

# ==============================================================================

.before_script: &before_script |
  export PATH="${CARGO_HOME}/bin:${PATH}"
  env | sort
  cargo --version || true
  rustc --version || true
  node --version || true
  npm --version || true

# ==============================================================================

lint:markdown:
  image: "${IMAGE_JAVASCRIPT}"
  stage: lint
  before_script:
    - *before_script
    - yarn global add markdownlint-cli
  script:
    - markdownlint --config .markdownlint.yml --ignore-path .markdownlintignore "**/*.md"

lint:shell:
  image: ${IMAGE_SHELLCHECK}
  stage: lint
  before_script:
    - *before_script
    - shellcheck --version
  script:
    - shellcheck --format=tty ./bin/*.sh

# ==============================================================================

build:mdbook:
  image: ${IMAGE_RUST}
  stage: build
  before_script:
    - *before_script
  script:
    - apk add --no-cache musl-dev
    - cargo install mdbook
    - mdbook --version

# ==============================================================================

test:pages:
  image: ${IMAGE_RUST}
  stage: test
  before_script:
    - *before_script
  script:
    - mdbook test

# ==============================================================================

pages:
  image: ${IMAGE_RUST}
  stage: deploy
  artifacts:
    expire_in: 42 yrs
    paths:
      - public/
  before_script:
    - *before_script
  cache:
    <<: *global_cache
    policy: pull
  only:
    - main
  script:
    - mdbook build
    - mv -v book public

# This job essentially deploys the website as a GitLab artifact with the name of
# the branch as a new "Environment."  To view the branch as a deployed website:
#   1.  Open the repository in a browser.
#   2.  Open Operations > Environments.
#   3.  Click on the "review/<branch_name> that appears under the Available
#       environments.
#   4.  In the upper-right, click the link "..................."
# Alternatively, if you already have the "test_branch" Job open in a browser,
# then there should be a link at the top of the Job's output saying something to
# the affect of "This job is deployed to review/<branch_name>."
# The idea of using "GitLab Environments" over actually deploying the site to a
# branch specific directory came from:
#   https://stackoverflow.com/questions/55596789/deploying-gitlab-pages-for-different-branches
#   https://docs.gitlab.com/ee/ci/environments/
#   https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
review:pages:
  image: ${IMAGE_RUST}
  stage: deploy
  artifacts:
    expire_in: 1 week
    paths:
      - public
  before_script:
    - *before_script
  cache:
    <<: *global_cache
    policy: pull
  environment:
    auto_stop_in: 1 week
    name: review/${CI_COMMIT_REF_NAME}
    url: "${CI_JOB_URL}/artifacts/file/public/index.html"
  except:
    - main
  script:
    - mdbook build
    - mv -v book public

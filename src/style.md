# EPT Writing Style

As you read articles on EPT there will be, more often than not, times when code
or commands will need to be displayed.  As EPT's goal is to briefly showcase
many Linux and Unix based command line tools there will be many ways the article
contents will be styled.  It should be clear what the styling means, so this
page can be used as a reference for how and why things are formatted the way
that they are.

## Running Commands

Since EPT is focused around the command line, it should be crystal clear how a
command will be run and how sample output will appear.  A command and its output
will be indented, dimmed, and monospaced as such:

```text
$ ls -al
total 56
drwxr-xr-x 10 devenv devenv 4096 May  5 00:33 .
drwxr-xr-x  1 root   root   4096 May  4 16:45 ..
-rw-------  1 devenv devenv  125 May  8 01:44 .bash_history
-rw-r--r--  1 devenv devenv  220 Feb 25 12:03 .bash_logout
-rw-r--r--  1 devenv devenv 3771 Feb 25 12:03 .bashrc
drwxr-xr-x  3 devenv devenv 4096 May  4 17:42 .bundle
drwxr-xr-x  6 devenv devenv 4096 May  4 17:36 .cache
drwxr-xr-x  6 devenv devenv 4096 May  9 18:10 .config
drwxr-xr-x  5 devenv devenv 4096 May  4 17:45 .java
drwxr-xr-x  5 devenv devenv 4096 May  4 17:32 .local
drwx------  5 devenv devenv 4096 May  4 17:33 .mozilla
-rw-r--r--  1 devenv devenv  807 Feb 25 12:03 .profile
drwx------  2 devenv devenv 4096 May  4 17:32 .ssh
drwx------  2 devenv devenv 4096 May  4 17:32 Downloads
```

Generally it will be obvious this is output from a command since it will have
the typical `$` before the command that will be ran.  Everything else should be
assumed is the output from the command.

Another note about running commands is that EPT will never implicitly run
commands as root.  If root is required for a particular situation then the
article will explicitly invoke `sudo` in front of all the commands requiring
elevated privileges.

```text
$ sudo ls -al /root
total 32
drwx------ 1 root root 4096 May  4 17:23 .
drwxr-xr-x 1 root root 4096 May  9 16:49 ..
-rw-r--r-- 1 root root 3106 Dec  5 14:39 .bashrc
drwxr-xr-x 1 root root 4096 May  4 17:22 .cache
drwxr-xr-x 3 root root 4096 May  4 17:23 .gem
-rw-r--r-- 1 root root  161 Dec  5 14:39 .profile
```

While some online tutorials will implicitly use a `#` symbol to denote a root
shell, such as:

```text
# ls -al /root
total 32
drwx------ 1 root root 4096 May  4 17:23 .
drwxr-xr-x 1 root root 4096 May  9 16:49 ..
-rw-r--r-- 1 root root 3106 Dec  5 14:39 .bashrc
drwxr-xr-x 1 root root 4096 May  4 17:22 .cache
drwxr-xr-x 3 root root 4096 May  4 17:23 .gem
-rw-r--r-- 1 root root  161 Dec  5 14:39 .profile
```

This can easily be missed, and lead to confusion.  EPT will keep this notation
to a minimum in preference for using `sudo` from a non-root shell.

## Code Formatting

### Code Blocks

In some instances, EPT will need to present code to the reader.  More often than
not this will be shell code, but it can be from any language if needed.  Code
blocks will be highlighted based on the language they are written in, and will
container a gutter with line numbers.

For example, a segment of Bash code will look something like this:

```bash
echo "Lorem ipsum dolor sit amet, ..." | grep Lorem

echo "This is some bash code"
echo "that spans"
echo "multiple lines"

# Convert a file from the DOS format, cause... who uses the DOS format anyway ;)
dos2unix dosFormattedFile.txt &>/dev/null
```

Probably the most important aspect of a code block is the copy button in the
upper right corner.  When clicked, the reader will have copied the code to their
clipboard.

### Inline Code

There will also be times when a short or small segment of code needs to be
accented, but does not warrant a full code block.  The code will be emphasized
like `this`, which will not display any kind of special syntax highlighting.

## Content Formatting

### Paragraphs

There is not much to say about paragraphs other than each article will try to
keep each paragraph short and to the point.  After all, each article is only
going to _talk about 20% of a tool_, right?

### Lists

Bulleted lists will be formatted as such:

* one
  * one
* two
  * two two
* three
  * four
    * five

And ordered lists will look similar, but with numerals:

1. item #1
1. item #2
    1. sub item #1
        1. sub sub item #1
    1. sub item #2
1. item #3

### Quotes

Text quotes will look similar to running commands and their output, however they
will not be monospaced.

> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
> nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
> eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
> in culpa qui officia deserunt mollit anim id est laborum.

However, since text quotes will look similar to running commands, these kinds of
quotes will generally be kept to a minimum.

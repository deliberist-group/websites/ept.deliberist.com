<!-- markdownlint-disable-file no-empty-links -->

# Summary

[Introduction](introduction.md)
[Style](style.md)

----

- [Automation]()
  - [GitLab CI]()
- [Data Display]()
  - [python -m json.tool]()
  - [tree]()
- [Data Manipulation]()
  - [awk]()
  - [sort]()
- [Data Transfer]()
  - [curl]()
  - [rsync]()
  - [scp]()
  - [wget]()
- [Editors]()
  - [vim](articles/editors/vim.md)
- [Mouseless User](articles/mouseless/user.md)
  - [i3](articles/mouseless/i3.md)
  - [Microsoft Windows](articles/mouseless/microsoft-windows.md)
  - [tmux](articles/mouseless/tmux.md)
- [Lanugages]()
  - [Python]()
- [Shells]()
  - [bash]()
  - [ssh](articles/shells/ssh.md)
- [Task Management]()
  - [systemctl]()
- [Window Managers]()
  - [i3]()

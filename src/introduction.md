
# EPT, or "80% There!"

![Pareto Principle](/images/pareto-principle-80-20-rule-bar-graph.png)

The terminal is a complex system, opening up the user to a plethora of even more
complex tools.  The number of tools accessible through the terminal is ever
growing.  Sometimes these tools have a near impossibility to master, they have
hundreds if not thousands of features.  Surely this is what makes the terminal
infamous as only be reserved for only the most nerdiest, only the most extremely
apt engineers.  This is certainly not the case.  This is where EPT comes in.
EPT, or _80% There_ embodies the
[Pareto Principle](https://en.wikipedia.org/wiki/Pareto_principle),
also known as the 80/20 rule.

The tools behind the terminal are complex, but what if we only learned 20% of
what is there.  What if we put in the effort to learn only 20% of a tool, only
20% of its features, only 20% of the config file, only 20% of the CLI options?
By learning only 20% of these tools, I believe, you will have everything you
need to know to build your 80% solution.

Developing a 100% solution right out of the gate is almost always doomed to
fail.  The 100% solution requires error handling, it requires knowledge of edge
cases, boundary conditions, faulty input, timeout conditions, and may other
facets.  In many cases, the time required to complete a job is exponential.
Results are seen very quickly in the project, but you'll quickly experience
diminishing returns the longer the project is developed.  The time required to
complete an additional task is orders of magnitude longer to complete simply
because the project has grown in size.

EPT is here to help get the 80% solution, and get it quickly!  I believe this
can be done simply by learning 20% of the tools you need for the project.  20%
effort should get you 80% results.

An analogy could be an iceberg.  In a boat or a ship we can see the top 20% of
an iceberg.  We do not **_need_** to see the full 100% of the iceberg, we just
need to know where the iceberg is, and avoid it!  By seeing the top 20% of the
iceberg, that gives us enough information to steer clear.

EPT will be a constant work in progress.  It will never be done.  If you wish to
receive notifications when EPT is updated, be sure to click the bell on the the
[Git repository](https://ept.deliberist.com/).

EPT is written like a reference manual, but is not any kind of definitive guide
in the strictest sense and can certainly be subjective at times.  If you want to
have a discussion about this page then I encourage you to start a discussion
using
[EPT's GitLab Issues](https://gitlab.com/deliberist-group/websites/ept.deliberist.com/-/issues)
, as it may result in positive changes for the EPT project.

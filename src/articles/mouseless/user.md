# A mouseless whhhhhat?!

In the mid- to late-60's, the first computer mouse was invented and demonstrated
to the public.   While revolutionary for the time, some might argue the computer
mouse was not popularized until the 1980's when the idea of a "_home computer_"
became more reified.  Since then, computer mice have become iconic with modern
life, and thus became extremely complicated with seemingly gluttonous features
that can make the device cost hundreds of USD.  Seriously, why are some so much
for just a couple of buttons?!

For the vast majority of the early days in computing, mice just simply were not
a thing.  They just were not as mainstream as they have become today.  The
command line is centered around a mouseless environment.  Text editors like vim,
emacs, pico/nano, and many others were created because mice were not available
to the overall computing population.

The mouse is not all bad.  There is no way we could have specialized fields like
graphic design, CAD software, audio/video editing, and much more!  So why give
up the mouse?

There is a slow growing movement in software development to go back to the old
days of a mouseless development environment.  Some argue their productivity is
hindered when they need to use a mouse.

## The why...

The computer mouse is highly inefficient when it comes to productivity.  Think
about it, a person needs to:

1. move their hand away from the keyboard,
1. optionally shake the mouse to discover it on the screen,
1. move it at high speed to a general area where they want to click,
1. move the mouse to the precise location which is often a relatively small area
    on the screen, compared to the screen's full resolution, and
1. finally make the click (and if you're anything like me, you hope you didn't
    miss, cause, ya'know aging eyes!)

However, with a keyboard one can imaging the TAB key acts much like the mouse.
We use the TAB key to change focus on different elements on the screen.  We move
between buttons and checkboxes to text areas and links.  The key difference is
the TAB key is exact!  Precision is not really needed as focus _snaps_ to the
next possible element.

## The how!

For some, the productivity increase of a mouseless environment can range from
marginal to maximal.  Others may see their productivity decrease.  At the end of
the day a mouseless environment is only as good as the user wishing to embrace
it.

Are you convinced?  Do you want to try a mouseless environment?  It sounds great
and all but how would we actually go completely mouseless?  With the next series
of "_mouseless articles_" I want to give you the tools to try it out.  I will
try to showcase the major systems that developers, system admins, and everyone
in between use.  I want to give you 20% of what is needed to use these systems
productively without a mouse.

**But first step #1:  unplug your mouse.**  Seriously, there will be some
struggling, but it is the only way to succeed.  Trust me, once all of my
mouseless articles are written I hope that you have all the tools needed to go
fully mouseless from login to your daily workflow.
